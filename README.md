mkdir sources && cd sources
wget https://ftp.gnu.org/gnu/mpfr/mpfr-4.1.0.tar.xz \
https://ftp.gnu.org/gnu/gmp/gmp-6.2.1.tar.xz \
https://ftp.gnu.org/gnu/mpc/mpc-1.2.1.tar.gz \
https://ftp.gnu.org/gnu/binutils/binutils-2.37.tar.xz \
https://ftp.gnu.org/gnu/glibc/glibc-2.34.tar.xz \
https://ftp.gnu.org/gnu/gcc/gcc-11.2.0/gcc-11.2.0.tar.xz \
https://libisl.sourceforge.io/isl-0.24.tar.xz \
http://sourceware.org/pub/newlib/newlib-4.1.0.tar.gz \
https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.15.1.tar.xz \
https://ftp.gnu.org/gnu/gdb/gdb-11.1.tar.xz \
https://gcc.gnu.org/pub/gcc/infrastructure/cloog-0.18.1.tar.gz \

wget https://ftp.gnu.org/gnu/mpfr/mpfr-4.1.0.tar.xz.sig \
https://ftp.gnu.org/gnu/gmp/gmp-6.2.1.tar.xz.sig \
https://ftp.gnu.org/gnu/mpc/mpc-1.2.1.tar.gz.sig \
https://ftp.gnu.org/gnu/binutils/binutils-2.37.tar.xz.sig \
https://ftp.gnu.org/gnu/glibc/glibc-2.34.tar.xz.sig \
https://ftp.gnu.org/gnu/gcc/gcc-11.2.0/gcc-11.2.0.tar.xz.sig \
https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.15.1.tar.sign \
https://ftp.gnu.org/gnu/gdb/gdb-11.1.tar.xz.sig


```sh
sha512sum -c sha512.sum --ignore-missing
for i in *.sig; do gpg2 --auto-key-retrieve --verify-files "${i}"; done
```

mkdir extracted && cd extracted

```sh
for i in ../sources/*.tar.*z; do unar $i; done
```

sudo apt install libgmp-dev

cd ../../build
mkdir -p ./build/binutils-build
mkdir -p ./build/gcc-build
mkdir -p ./build/newlib-build
mkdir -p ./build/gdb-build


```sh
cd binutils-2.30
ln -s ../isl-0.18 isl
cd ..
cd gcc-8.1.0
ln -s ../isl-0.18 isl
ln -s ../mpfr-4.0.1 mpfr
ln -s ../gmp-6.1.2 gmp
ln -s ../mpc-1.1.0 mpc
ln -s ../cloog-0.18.1 cloog
cd ..
```


../../sources/extracted/binutils-2.37/configure --prefix=/usr/local/cross-compiler --target=aarch64-elf --enable-shared --enable-threads=posix --enable-libmpx --with-system-zlib --with-isl --enable-__cxa_atexit --disable-libunwind-exceptions --enable-clocale=gnu --disable-libstdcxx-pch --disable-libssp --enable-plugin --disable-linker-build-id --enable-lto --enable-install-libiberty --with-linker-hash-style=gnu --with-gnu-ld --enable-gnu-indirect-function --disable-multilib --disable-werror --enable-checking=release --enable-default-pie --enable-default-ssp --enable-gnu-unique-object
make -j4
sudo make install

export PATH="$PATH:/usr/local/cross-compiler/bin"

../../sources/extracted/gcc-11.2.0/configure --prefix=/usr/local/cross-compiler --target=aarch64-elf --enable-languages=c --without-headers --with-newlib --enable-shared --enable-threads=posix --enable-libmpx --with-system-zlib --with-isl --enable-__cxa_atexit --disable-libunwind-exceptions --enable-clocale=gnu --disable-libstdcxx-pch --disable-libssp --enable-plugin --disable-linker-build-id --enable-lto --enable-install-libiberty --with-linker-hash-style=gnu --with-gnu-ld --enable-gnu-indirect-function --disable-multilib --disable-werror --enable-checking=release --enable-default-pie --enable-default-ssp --enable-gnu-unique-object 

make -j4 all-gcc
sudo make all-gcc install-gcc 2>&1 | tee ./gcc-build-withoutnewlib-logs.log

cd ~/toolchain/build/newlib-build

../../sources/newlib-2.5.0/configure --target=aarch64-elf --prefix=/usr/local/cross-compiler --disable-newlib-supplied-syscalls

make -j4
# sudo make install

../../sources/extracted/gcc-11.2.0/configure --prefix=/usr/local/cross-compiler --target=aarch64-elf --enable-languages=c,c++--with-newlib --enable-shared --enable-threads=posix --enable-libmpx --with-system-zlib --with-isl --enable-__cxa_atexit --disable-libunwind-exceptions --enable-clocale=gnu --disable-libstdcxx-pch --disable-libssp --enable-plugin --disable-linker-build-id --enable-lto --enable-install-libiberty --with-linker-hash-style=gnu --with-gnu-ld --enable-gnu-indirect-function --disable-multilib --disable-werror --enable-checking=release --enable-default-pie --enable-default-ssp --enable-gnu-unique-object 

sudo make all-gcc install-gcc 2>&1 | tee ./gcc-build-withnewlib-logs.log

Congratulation your toolchain is now fully functional with only one exception i.e. it doesn't have Debugging support. So let's add that support as well to the our newly born baby toolchain.



https://gist.github.com/gofer/79d3e09290172b55f8c75d58e1be0502

https://archive.md/u9OL8

https://community.arm.com/support-forums/f/armds-forum/43925/how-to-use-aarch64-elf-gcc-to-print-hello-world

https://pkgsrc.se/cross/aarch64-none-elf-gcc


https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads

export PATH="$PATH:/usr/local/cross-compiler/bin"
arm-none-eabi-gcc --specs=nosys.specs -o main main.c

https://github.com/F1r3Hydr4nt/arm-newlib-gcc-toolchain-builder

https://releases.linaro.org/components/toolchain/gcc-linaro/7.5-2019.12/
https://archive.md/wip/9KaqN
https://balau82.wordpress.com/2010/12/16/using-newlib-in-arm-bare-metal-programs/
https://wiki.osdev.org/Porting_Newlib